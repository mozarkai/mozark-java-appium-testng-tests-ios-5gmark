package tests;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import io.appium.java_client.MobileElement;

public class RandomTest extends BaseClass{

	@Test
	public void changeDataUnits() {
		try {

			// Changing data units to MBps
			Thread.sleep(10000);
			WebDriverWait wait5 = new WebDriverWait(driver, 10);
			wait5.until(ExpectedConditions.visibilityOfElementLocated(By.name("TEST")));
			MobileElement ios13Settings = driver.findElement(By.xpath("//XCUIElementTypeApplication[@name=\"5gmark\"]/XCUIElementTypeWindow[1]//XCUIElementTypeButton[5]"));
			try {
				MobileElement ios14Settings = driver.findElement(By.xpath("//XCUIElementTypeTabBar[@name=\"Tab Bar\"]/XCUIElementTypeButton[5]"));
				if(ios14Settings.isDisplayed()) {
					ios14Settings.click();	
				}					
			} catch (Exception e) {
				ios13Settings.click();
			}

			WebDriverWait wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("MBps")));
			driver.findElement(By.id("MBps")).click();
			MobileElement ios13Test = driver.findElement(By.xpath("//XCUIElementTypeApplication[@name=\"5gmark\"]/XCUIElementTypeWindow[1]//XCUIElementTypeTabBar/XCUIElementTypeButton[3]"));
			try {
				MobileElement ios14Test = driver.findElement(By.xpath("//XCUIElementTypeTabBar[@name=\"Tab Bar\"]/XCUIElementTypeButton[3]"));
				if (ios14Test.isDisplayed()) {
					ios14Test.click();
				}

			}catch (Exception e) {
				ios13Test.click();
			}
			WebDriverWait wait6 = new WebDriverWait(driver, 10);
			wait6.until(ExpectedConditions.visibilityOfElementLocated(By.name("Speed test")));
			driver.findElement(By.name("Speed test")).click();
			driver.findElement(By.name("TEST")).click();
			WebDriverWait wait1 = new WebDriverWait(driver, 10);
			wait1.until(ExpectedConditions.visibilityOfElementLocated(By.name("Indoor")));
			driver.findElement(By.name("Indoor")).click();

			// Waiting for results page
			WebDriverWait wait2 = new WebDriverWait(driver, 180);
			wait2.until(ExpectedConditions.visibilityOfElementLocated(By.name("Results")));
			driver.findElement(By.name("Explanation")).click();

			// Printing results
			WebDriverWait wait3 = new WebDriverWait(driver, 10);
			wait3.until(ExpectedConditions.visibilityOfElementLocated(By.name("DOWNLOAD")));

			try {
				MobileElement download = driver.findElement(By.xpath("//*[starts-with(@name, 'My download bitrate')]"));
				if (download.isDisplayed()) {
					String downloadResult = download.getText();
					System.out.println(downloadResult);
				}

				MobileElement upload = driver.findElement(By.xpath("//*[starts-with(@name, 'My upload bitrate')]"));
				if (upload.isDisplayed()) {
					String uploadText = upload.getText();
					System.out.println(uploadText);
				}
				
			} catch (Exception e) {
				System.out.println("Not displaying result as this is iOS 13 device");
			}

			// Verifying whether result is in MBps
			driver.findElement(By.name("Results")).click();
			try {
				MobileElement result = driver.findElement(By.xpath("//*[contains(@name,\"MBps\")]"));
				if (result.isDisplayed()) {
					String resultText = result.getText();
					System.out.println(resultText);
					Assert.assertTrue(true);
				}
			} catch(Exception e) {
				System.out.println("Not displaying result as this is iOS 13 device");
			}

			// Reverting changes
			System.out.println("Reverting data unit changes");
			try {
				MobileElement ios14Back = driver.findElement(By.xpath("//XCUIElementTypeNavigationBar[@name=\"_gmark_crowd.GMCResultsPageView\"]/XCUIElementTypeButton"));
				ios14Back.click();
				MobileElement ios14Settings1 = driver.findElement(By.xpath("//XCUIElementTypeTabBar[@name=\"Tab Bar\"]/XCUIElementTypeButton[5]"));
				if(ios14Settings1.isDisplayed()) {
					ios14Settings1.click();	
				}					
			} catch (Exception e) {
				driver.terminateApp("com.apple.Preferences");
				driver.terminateApp("io.appium.android.apis");
				driver.launchApp();
				WebDriverWait wait4 = new WebDriverWait(driver, 10);
				wait4.until(ExpectedConditions.visibilityOfElementLocated(By.name("TEST")));
				MobileElement ios13Settings1 = driver.findElement(By.xpath("//XCUIElementTypeApplication[@name=\"5gmark\"]/XCUIElementTypeWindow[1]//XCUIElementTypeButton[5]"));
				ios13Settings1.click();
			}
			WebDriverWait wait4 = new WebDriverWait(driver, 10);
			wait4.until(ExpectedConditions.visibilityOfElementLocated(By.id("Mbps")));
			driver.findElement(By.id("Mbps")).click();

		}catch(Exception exp) {
			System.out.println("Cause is: "+exp.getCause());
			System.out.println("Message is: "+exp.getMessage());
			exp.printStackTrace();
			Assert.assertTrue(false);
		}

	}

	@Test
	public void speedOutdoorTest() {
		try {
			// Changing data units to MBps
			Thread.sleep(10000);
			WebDriverWait wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.name("Speed test")));
			driver.findElement(By.name("Speed test")).click();
			driver.findElement(By.name("TEST")).click();
			WebDriverWait wait1 = new WebDriverWait(driver, 10);
			wait1.until(ExpectedConditions.visibilityOfElementLocated(By.name("Outdoor")));
			driver.findElement(By.name("Outdoor")).click();

			// Waiting for results page
			WebDriverWait wait2 = new WebDriverWait(driver, 180);
			wait2.until(ExpectedConditions.visibilityOfElementLocated(By.name("Results")));
			driver.findElement(By.name("Explanation")).click();

			// Printing results
			WebDriverWait wait3 = new WebDriverWait(driver, 10);
			wait3.until(ExpectedConditions.visibilityOfElementLocated(By.name("DOWNLOAD")));
			try {
				MobileElement download = driver.findElement(By.xpath("//*[starts-with(@name, 'My download bitrate')]"));
				if (download.isDisplayed()) {
					String downloadResult = download.getText();
					System.out.println(downloadResult);
				}

				MobileElement upload = driver.findElement(By.xpath("//*[starts-with(@name, 'My upload bitrate')]"));
				if (upload.isDisplayed()) {
					String uploadText = upload.getText();
					System.out.println(uploadText);
				}
				Assert.assertTrue(true);
			} catch (Exception e) {
				System.out.println("Not displaying result as this is iOS 13 device");
			}
		}catch(Exception e) {
			e.printStackTrace();
			Assert.assertTrue(false);
		}

	}
}