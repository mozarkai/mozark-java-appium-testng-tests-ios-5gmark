package tests;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import io.appium.java_client.MobileElement;

public class FullTest extends BaseClass{

	@Test
	public void fullTestIndoor() {
		try {
			Thread.sleep(10000);

			// Starting indoor full test
			WebDriverWait wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.name("TEST")));
			driver.findElement(By.name("TEST")).click();
			WebDriverWait wait1 = new WebDriverWait(driver, 10);
			wait1.until(ExpectedConditions.visibilityOfElementLocated(By.name("Indoor")));
			driver.findElement(By.name("Indoor")).click();

			// Waiting for results page
			WebDriverWait wait2 = new WebDriverWait(driver, 240);
			wait2.until(ExpectedConditions.visibilityOfElementLocated(By.name("Results")));
			driver.findElement(By.name("Explanation")).click();

			// Printing results
			try {
				WebDriverWait wait3 = new WebDriverWait(driver, 10);
				wait3.until(ExpectedConditions.visibilityOfElementLocated(By.name("SCORE")));

				String score = driver.findElement(By.xpath("//*[starts-with(@name, 'My Score')]")).getText();
				System.out.println(score);

				String download = driver.findElement(By.xpath("//*[starts-with(@name, 'My download bitrate')]")).getText();
				System.out.println(download);

				String upload = driver.findElement(By.xpath("//*[starts-with(@name, 'My upload bitrate')]")).getText();
				System.out.println(upload);

				String stream = driver.findElement(By.xpath("//*[starts-with(@name, 'No lag did')]")).getText();
				System.out.println(stream);

				String web = driver.findElement(By.xpath("//*[starts-with(@name, 'Average time to load')]")).getText();
				System.out.println(web);				
			} catch (Exception e) {
				System.out.println("Not displaying result as this is iOS 13 device");
			}


			// Navigating to history tab
			try {
				MobileElement ios14Back = driver.findElement(By.xpath("//XCUIElementTypeNavigationBar[@name=\"_gmark_crowd.GMCResultsPageView\"]/XCUIElementTypeButton"));
				ios14Back.click();
				MobileElement ios14Settings1 = driver.findElement(By.xpath("//XCUIElementTypeTabBar[@name=\"Tab Bar\"]/XCUIElementTypeButton[5]"));
				if(ios14Settings1.isDisplayed()) {
					ios14Settings1.click();	
				}		
				driver.findElement(By.xpath("//XCUIElementTypeTabBar[@name=\"Tab Bar\"]/XCUIElementTypeButton[2]")).click();

			} catch (Exception e) {
				driver.terminateApp("com.apple.Preferences");
				driver.terminateApp("io.appium.android.apis");
				driver.launchApp();
				WebDriverWait wait4 = new WebDriverWait(driver, 10);
				wait4.until(ExpectedConditions.visibilityOfElementLocated(By.name("TEST")));
				driver.findElement(By.xpath("//XCUIElementTypeApplication[@name=\"5gmark\"]/XCUIElementTypeWindow[1]//XCUIElementTypeTabBar/XCUIElementTypeButton[2]")).click();
			}

			// Clicking on the first result
			Thread.sleep(5000);
			driver.findElement(By.xpath("//XCUIElementTypeWindow[1]//XCUIElementTypeOther[1]//XCUIElementTypeCell[1]")).click();
			WebDriverWait wait4 = new WebDriverWait(driver, 10);
			wait4.until(ExpectedConditions.visibilityOfElementLocated(By.name("Results")));

			// Verifying whether result page is displayed
			if(driver.findElement(By.name("Results")).isDisplayed()) 
				Assert.assertTrue(true);

		}catch(Exception exp) {
			System.out.println("Cause is: "+exp.getCause());
			System.out.println("Message is: "+exp.getMessage());
			exp.printStackTrace();
			Assert.assertTrue(false);
		}
	}
}